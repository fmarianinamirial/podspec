Pod::Spec.new do |s|
  s.name         = "Namirial.Intro"
  s.version      = "1.0.0"
  s.summary      = "Namirial.Intro SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.namirial.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/Namirial.Intro.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.ios.vendored_frameworks = "Namirial_Intro.framework"
  s.requires_arc = true
	
end
