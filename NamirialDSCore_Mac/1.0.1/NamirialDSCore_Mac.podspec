Pod::Spec.new do |s|
  s.name         = "NamirialDSCore_Mac"
  s.version      = "1.0.1"
  s.summary      = "NamirialDS SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.osx.deployment_target = '10.12'
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialDSCore_Mac.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.osx.vendored_frameworks = "NamirialDSCore.framework"
  s.requires_arc = true

end
