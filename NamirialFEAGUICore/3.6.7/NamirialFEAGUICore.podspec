Pod::Spec.new do |s|
  s.name         = "NamirialFEAGUICore"
  s.version      = "3.6.7"
  s.summary      = "NamirialFEAGUICore SDK."
  s.description  = "Framework Viewer Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialFEAGUICore.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '-lObjC'
  }
  s.ios.library = 'c++', 'iconv', 'z'
  s.ios.vendored_frameworks = "NamirialFEAGUICore.framework"
  s.requires_arc = true

end
