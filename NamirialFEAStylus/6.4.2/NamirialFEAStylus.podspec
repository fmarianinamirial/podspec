Pod::Spec.new do |s|
  s.name         = "NamirialFEAStylus"
  s.version      = "6.4.2"
  s.summary      = "NamirialFEAStylus SDK"
  s.description  = "Framework Namirial for Stylus"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialFEAStylus.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.ios.vendored_frameworks = "NamirialFEAStylus.framework"
  s.requires_arc = true

end
