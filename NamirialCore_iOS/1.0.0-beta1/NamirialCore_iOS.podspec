Pod::Spec.new do |s|
  s.name         = "NamirialCore_iOS"
  s.version      = "1.0.0-beta1"
  s.summary      = "NamirialCore"
  s.description  = "Framework contentente la libreria SBB"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.ios.deployment_target = '9.0'
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialCore_iOS.git",
    :tag => "#{s.version}"
  }
  s.pod_target_xcconfig = {
    'ENABLE_BITCODE' => 'NO'
  }
  s.user_target_xcconfig = {
    'ENABLE_BITCODE' => 'NO'
  }
  s.ios.vendored_frameworks = "NamirialCore.framework"
  s.requires_arc = true

end
