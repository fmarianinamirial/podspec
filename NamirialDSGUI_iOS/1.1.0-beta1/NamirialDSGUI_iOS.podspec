Pod::Spec.new do |s|
  s.name         = "NamirialDSGUI_iOS"
  s.version      = "1.1.0-beta1"
  s.summary      = "NamirialDSGUI SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.ios.deployment_target = '9.0'
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialDSGUI_iOS.git",
    :branch => "master",
    :tag => "1.1.0-beta1"
  }
  s.pod_target_xcconfig = {
    'ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES' => 'YES',
    'ENABLE_BITCODE' => 'NO'
  }
  s.user_target_xcconfig = {
    'ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES' => 'YES',
    'ENABLE_BITCODE' => 'NO'
  }
  s.ios.vendored_frameworks = "NamirialDSGUI.framework"
  s.requires_arc = true
end
