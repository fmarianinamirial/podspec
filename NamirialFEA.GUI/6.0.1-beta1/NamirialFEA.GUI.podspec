Pod::Spec.new do |s|
  s.name         = "NamirialFEA.GUI"
  s.version      = "6.0.1-beta1"
  s.summary      = "NamirialFEA.GUI SDK."
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialFEA.GUI.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.ios.dependency 'NamirialFEA', '6.0.1-beta1'
  s.ios.vendored_frameworks = "NamirialFEA.GUI.framework"
  s.requires_arc = true
end
