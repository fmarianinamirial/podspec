Pod::Spec.new do |s|
  s.name         = "NamirialFEA.GUI"
  s.version      = "6.11.2-beta1"
  s.summary      = "NamirialFEAGUI SDK."
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialFEA.GUI.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.pod_target_xcconfig = {
    'ENABLE_BITCODE' => 'NO'
  }
  s.user_target_xcconfig = {
    'ENABLE_BITCODE' => 'NO'
  }
  s.ios.library = 'c++'
  s.ios.framework = 'QuartzCore'
  s.ios.dependency 'NamirialFEA', '6.11.2-beta1'
  s.ios.vendored_frameworks = "NamirialFEAGUI.framework"
  s.requires_arc = true

end
