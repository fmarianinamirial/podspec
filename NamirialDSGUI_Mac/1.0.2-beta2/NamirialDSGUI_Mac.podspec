Pod::Spec.new do |s|
  s.name         = "NamirialDSGUI_Mac"
  s.version      = "1.0.2-beta2"
  s.summary      = "NamirialDSGUI SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.osx.deployment_target = '10.12'
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialDSGUI_Mac.git",
    :branch => "master",
    :tag => "1.0.2-beta2"
  }
  s.osx.vendored_frameworks = "NamirialDSGUI.framework"
  s.requires_arc = true
end
