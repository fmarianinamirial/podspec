Pod::Spec.new do |s|
  s.name         = "NamirialPDFEditing"
  s.version      = "1.0.0"
  s.summary      = "NamirialPDFEditing framework"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/Namirial.PdfEditing.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '-lc++'
  }
  s.ios.framework = 'CoreText'
  s.ios.library = 'xml2'
  s.ios.vendored_frameworks = "NamirialPdfEditing.framework"

end
