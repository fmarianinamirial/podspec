Pod::Spec.new do |s|
  s.name         = "NamirialDataSharing"
  s.version      = "4.0.1"
  s.summary      = "NamirialDataSharing"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialDataSharing.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '-lObjC'
  }
  s.ios.vendored_frameworks = "NamirialDataSharing.framework"
  s.requires_arc = true

end
