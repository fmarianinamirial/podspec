Pod::Spec.new do |s|
  s.name         = "NamirialCore_Mac"
  s.version      = "1.0.0-beta1"
  s.summary      = "NamirialCore"
  s.description  = "Framework Core per la crittografia"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.osx.deployment_target = '10.9'
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialCore_Mac.git",
    :tag => "#{s.version}"
  }
  s.osx.vendored_frameworks = "NamirialCore.framework"
  s.requires_arc = true

end
