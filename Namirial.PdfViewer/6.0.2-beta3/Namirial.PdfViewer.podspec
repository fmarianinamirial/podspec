Pod::Spec.new do |s|
  s.name         = "Namirial.PdfViewer"
  s.version      = "6.0.2-beta3"
  s.summary      = "NamirialFEA SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/Namirial.PdfViewer.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.ios.dependency 'NamirialFEA.GUI', '6.0.2-beta3'
  s.ios.vendored_library = 'libNamirial.PdfViewer.a'
  s.resource = "Namirial.PdfViewer.Resources.bundle"
  s.source_files = "Headers/*.{h,m}"
  s.public_header_files = "Headers/*.h"

end
