Pod::Spec.new do |s|
  s.name         = "NamirialFEA.GUI"
  s.version      = "XXX_XXX"
  s.summary      = "NamirialFEAGUI SDK."
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :http => "https://nexmicro.namirial.it/ios/frameworks/namirialfeagui/XXX_XXX/NamirialFEAGUI.framework.zip" }
  s.pod_target_xcconfig = {
    'ENABLE_BITCODE' => 'NO'
  }
  s.user_target_xcconfig = {
    'ENABLE_BITCODE' => 'NO'
  }
  s.ios.library = 'c++'
  s.ios.framework = 'QuartzCore'
  s.ios.dependency 'NamirialFEA', 'XXX_XXX'
  s.ios.vendored_frameworks = "Carthage/Build/iOS/NamirialFEAGUI.framework"
  s.requires_arc = true

end
