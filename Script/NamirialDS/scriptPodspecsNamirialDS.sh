if [ $# -eq 0 ]
	then
		echo "No arguments supplied"
		exit 1
fi

if [ $# -ne 2 ]
	then
		echo "Usage scriptPodspecsNamirialDS.sh version pathToPodspecFolder"
		exit 1
fi

if [ ! -d $2 ]; then
	echo "Folder $2 not found"
	exit 1
fi

Podspec="NamirialDS"

if [ ! -d $2/$Podspec ]; then
	echo "Creating foler $Podspec in $2"
	mkdir $2/$Podspec
fi

echo "Creating $1 folder in $Podspec"
mkdir $2/$Podspec/$1
echo "Copying podspec in folder $Podspec/$1/"
cp $Podspec.podspec.json $2/$Podspec/$1/
sed -i '' -e 's/XXX_XXX/'$1'/g' $2/$Podspec/$1/$Podspec.podspec.json

Podspec="NamirialDSCore"

if [ ! -d $2/$Podspec ]; then
	echo "Creating foler $Podspec in $2"
	mkdir $2/$Podspec
fi

echo "Creating $1 folder in $Podspec"
mkdir $2/$Podspec/$1
echo "Copying podspec in folder $Podspec/$1/"
cp $Podspec.podspec.json $2/$Podspec/$1/
sed -i '' -e 's/XXX_XXX/'$1'/g' $2/$Podspec/$1/$Podspec.podspec.json

Podspec="NamirialDSGUI"

if [ ! -d $2/$Podspec ]; then
	echo "Creating foler $Podspec in $2"
	mkdir $2/$Podspec
fi

echo "Creating $1 folder in $Podspec"
mkdir $2/$Podspec/$1
echo "Copying podspec in folder $Podspec/$1/"
cp $Podspec.podspec.json $2/$Podspec/$1/
sed -i '' -e 's/XXX_XXX/'$1'/g' $2/$Podspec/$1/$Podspec.podspec.json
