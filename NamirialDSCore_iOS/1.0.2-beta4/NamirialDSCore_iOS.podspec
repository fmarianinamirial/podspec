Pod::Spec.new do |s|
  s.name         = "NamirialDSCore_iOS"
  s.version      = "1.0.2-beta4"
  s.summary      = "NamirialDS SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.ios.deployment_target = '9.0'
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialDSCore_iOS.git",
    :tag => "1.0.2-beta4"
  }
  s.ios.vendored_frameworks = "NamirialDSCore.framework"
  s.requires_arc = true

end
