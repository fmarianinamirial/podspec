Pod::Spec.new do |s|
  s.name         = "NamirialFEA"
  s.version      = "6.12.0-beta3"
  s.summary      = "NamirialFEA SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :http => "https://nexmicro.namirial.it/ios/frameworks/namirialfea/6.12.0-beta3/NamirialFEA.framework.zip" }
  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '-lObjC',
    'ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES' => 'YES'
  }
  s.user_target_xcconfig = {
    'ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES' => 'YES'
  }
  s.ios.dependency 'NamirialFEAStylus', '17.10.16'
  s.ios.dependency 'NamirialCore', '1.0.0'
  s.ios.library = 'c++', 'iconv'
  s.ios.framework = 'CoreBluetooth'
  s.ios.vendored_frameworks = "Carthage/Build/iOS/NamirialFEA.framework"
  s.requires_arc = true

end
