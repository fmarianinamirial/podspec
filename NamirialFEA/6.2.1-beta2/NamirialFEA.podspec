Pod::Spec.new do |s|
  s.name         = "NamirialFEA"
  s.version      = "6.2.1-beta2"
  s.summary      = "NamirialFEA SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialFEA.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '-lObjC',
    'EMBEDDED_CONTENT_CONTAINS_SWIFT' => 'YES',
    'ENABLE_BITCODE' => 'NO'
  }
  s.ios.library = 'c++', 'iconv'
  s.ios.vendored_frameworks = "NamirialFEA.framework"
  s.requires_arc = true

end
