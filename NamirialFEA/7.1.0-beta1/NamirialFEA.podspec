Pod::Spec.new do |s|
  s.name         = "NamirialFEA"
  s.version      = "7.1.0-beta1"
  s.summary      = "NamirialFEA SDK"
  s.description  = "Framework Namirial"
  s.homepage = "http://www.firmagrafometrica.it/"
  s.license      = { :type => "Commercial", :text => "Namirial S.p.A. Copyright 2016" }
  s.author       = { "Namirial S.p.A." => "graphosign.support@namirial.com" }
  s.platform     = :ios, "8.0"
  s.source       = {
    :git => "http://gitmicro.namirial.it/cocoapods/NamirialFEA.git",
    :branch => "master",
    :tag => "#{s.version}"
  }
  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '-lObjC',
    'ENABLE_BITCODE' => 'NO',
    'ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES' => 'YES'
  }
  s.user_target_xcconfig = {
    'ENABLE_BITCODE' => 'NO',
    'ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES' => 'YES'
  }
  s.ios.dependency 'NamirialFEAStylus', '17.10.16'
  s.ios.library = 'c++', 'iconv'
  s.ios.framework = 'CoreBluetooth'
  s.ios.vendored_frameworks = "NamirialFEA.framework"
  s.requires_arc = true

end
